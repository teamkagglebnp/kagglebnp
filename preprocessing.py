"""
Preprocessing script.
@auther Yann Carbonne adapted by Aurelien Galicher for BNP kaggle Challenge
You can create the preprocessed data and create a feature selector mask.
With the quick & dirty model, give 0.71556 on public leader board.
"""

import pandas as pd
import numpy as np
import xgboost as xgb
import datetime
import calendar
from sklearn.preprocessing import LabelEncoder, Imputer
from sklearn.cross_validation import StratifiedKFold
from sklearn.feature_selection import RFECV
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import log_loss
#%matplotlib inline
#import matplotlib.pyplot as plt;
from sklearn.cross_validation import cross_val_score
import xgboost as xgb
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler


def get_preproc(with_selector=False):
    """Load preprocessed data with or without the selector mask."""
    train, target, test = np.loadtxt('train.txt'), np.loadtxt('target.txt'), np.loadtxt('test.txt')
    if with_selector:
        selector = np.loadtxt('feature_selector_support.txt')
        selector = np.array(selector, dtype=bool)
        train, test = train[:, selector], test[:, selector]
    return train, target, test


def _create_preprocessing():
    train = pd.read_csv('train.csv')
    test = pd.read_csv('test.csv')
    submit = pd.read_csv('sample_submission.csv')
    df_all = pd.concat([train, test], axis=0)

    #################
    # Creatind additional features based on NA values
    #################
    na_columns = map(lambda x: x+'_na', df_all.columns)
    df_all_na = df_all.isnull().astype(int)
    df_all_na.columns = na_columns
    df_all = pd.concat([df_all, df_all_na], axis=1)
    
    #################
    # Sorting categorial and continous features.
    #################
    cat_features= train.select_dtypes(include=['object']).columns
    cont_features = train.select_dtypes(exclude=['object']).columns[2:] #removing id & target

    ##################
    # handling missing values
    # imputer NaN with most_frequent value for categorial features
    ##################

    most_frequent_cat_value = {}
    for feature in cat_features:
        most_frequent_cat_value[feature] = df_all[feature].value_counts().index[0]
    #print most_frequent_cat_value
    for feature in cat_features:
        df_all[feature] = df_all[feature].fillna(value=most_frequent_cat_value[feature])

    ##################
    # handling missing values
    # imputer mean for continuous features
    ##################

    imp = Imputer()
    df_all[cont_features] = imp.fit_transform(df_all[cont_features])


    #######################
    # "simple" label encoding of categorial features
    #######################
    le = preprocessing.LabelEncoder()
    le.fit(np.unique(df_all[cat_features]).ravel())
    df_all[cat_features] = le.transform(df_all[cat_features])

    ########################
    # Standard Sacling (-mean/std)
    ########################
    scaler = StandardScaler()
    df_all[cont_features.union(cat_features)] = scaler.fit_transform(df_all[cont_features.union(cat_features)])

    ########################
    # Recreate train / test / target
    train = df_all[df_all.target.notnull()].drop(['ID','target'], axis=1) 
    test = df_all[df_all.target.isnull()].drop(['ID','target'], axis=1) 
    index_test = df_all[df_all.target.isnull()]['ID'].values 
    target = df_all[df_all.target.notnull()].target.values

    # Save
    np.savetxt('train.txt', train, fmt='%s')
    np.savetxt('test.txt', test, fmt='%s')
    np.savetxt('target.txt', target, fmt='%s')


class MyXGB(xgb.XGBClassifier):
    """Scikit-Learn wrapper for XGBoost."""

    def __init__(self, max_depth=10, learning_rate=0.1, n_estimators=150,
                 silent=True, objective='binary:logistic',
                 nthread=-1, gamma=0, min_child_weight=1, max_delta_step=0,
                 subsample= 0.9, colsample_bytree=1, colsample_bylevel=1,
                 reg_alpha=0, reg_lambda=1, scale_pos_weight=1,
                 base_score=0.5, seed=42, missing=None):
        super(MyXGB, self).__init__(max_depth=10, learning_rate=0.1, n_estimators=150,
                 silent=True, objective='binary:logistic',
                 nthread=-1, gamma=0, min_child_weight=1, max_delta_step=0,
                 subsample= 0.9, colsample_bytree=1, colsample_bylevel=1,
                 reg_alpha=0, reg_lambda=1, scale_pos_weight=1,
                 base_score=0.5, seed=42, missing=None)
        

    def fit(self, X, y, eval_set=None, eval_metric=None,
            early_stopping_rounds=None, verbose=True):
        super(MyXGB, self).fit(X, y, eval_set=None, eval_metric=None,
            early_stopping_rounds=None, verbose=True)
        self.feature_importances_ = np.array(self.booster().get_fscore().values())
        return self

def myscorer(estimator, X, y):
    ypred = estimator.predict_proba(X)
    result = - log_loss(y, ypred)
    print result
    return result


def _create_selector():
    train, target, _ = get_preproc()
    train = train

    param = {'max_depth': 10, 'eta': 0.1, 'silent': 1, 'objective': 'multi:softprob',
             'lambda': 1, 'subsample': 0.9, 'eval_metric': 'logloss', 'num_class': 2}
    num_rounds = 15
    rfecv = RFECV(estimator=MyXGB(param, num_rounds), cv=StratifiedKFold(target, 2), scoring=myscorer)
    rfecv.fit(train, target)

    print("Optimal number of features : %d" % rfecv.n_features_)

    np.savetxt('feature_selector_support.txt', rfecv.support_, fmt='%s')

    # Plot number of features VS. cross-validation scores
    #plt.figure()
    #plt.xlabel("Number of features selected")
    #plt.ylabel("Cross validation score (nb of correct classifications)")
    #plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)

    #plt.show()